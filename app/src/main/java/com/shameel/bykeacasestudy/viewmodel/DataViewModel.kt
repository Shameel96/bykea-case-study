package com.shameel.bykeacasestudy.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shameel.bykeacasestudy.data.DataRepository
import com.shameel.bykeacasestudy.data.model.SearchResponse
import com.shameel.bykeacasestudy.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class DataViewModel @Inject constructor(
    private val repository: DataRepository
) : ViewModel() {

    val searchResponse: MutableLiveData<Resource<SearchResponse>> = MutableLiveData()

    fun getMusicItems(artistName: String) = viewModelScope.launch {
        getSearchResults(artistName)
    }

    private suspend fun getSearchResults(artistName: String) {
        searchResponse.postValue(Resource.Loading())
        try {
            val response = repository.getSearchResults(artistName = artistName)
            response.data?.let {
                searchResponse.postValue(Resource.Success(it))
            }
            searchResponse.postValue(response)
        } catch (t: Throwable) {
            when (t) {
                is IOException -> searchResponse.postValue(Resource.Error("Network Error"))
                else -> searchResponse.postValue(Resource.Error("Something went wrong"))
            }
        }
    }
}