package com.shameel.bykeacasestudy.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.shameel.bykeacasestudy.R
import com.shameel.bykeacasestudy.callback.MusicItemClickCallback
import com.shameel.bykeacasestudy.data.model.SearchResult
import com.shameel.bykeacasestudy.databinding.ItemMusicItemBinding

class MusicItemAdapter : RecyclerView.Adapter<MusicItemAdapter.ItemViewHolder>() {

    private var musicList = ArrayList<SearchResult>()
    private var callback: MusicItemClickCallback? = null

    inner class ItemViewHolder(
        private val parent: ViewGroup,
        private val binding: ItemMusicItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_music_item,
            parent,
            false
        )
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.itemView.setOnClickListener {
                callback?.onMusicItemClicked(item = musicList[adapterPosition])
                setIsPlayingFalse()
                musicList[adapterPosition].isPlaying = true
                notifyDataSetChanged()
            }
        }

        fun bind(item: SearchResult) {
            binding.ivArtist.loadImage(item.artworkUrl100)
            binding.tvSongName.text = item.trackName
            binding.tvArtist.text = item.artistName
            binding.tvAlbum.text = item.collectionName

            if (item.isPlaying) binding.lottiePlaying.visible() else binding.lottiePlaying.gone()
        }

    }

    private fun setIsPlayingFalse() {
        musicList.forEach { it.isPlaying = false }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(parent)
    }

    override fun getItemCount(): Int {
        return musicList.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(item = musicList[position])
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun clearList(){
        this.musicList = ArrayList()
        notifyDataSetChanged()
    }

    fun setImagesList(imageList: ArrayList<SearchResult>) {
        this.musicList = imageList
        notifyDataSetChanged()
    }

    fun setCallback(callback: MusicItemClickCallback) {
        this.callback = callback
    }

}
