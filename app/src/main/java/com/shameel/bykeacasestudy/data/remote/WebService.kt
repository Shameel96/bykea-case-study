package com.shameel.bykeacasestudy.data.remote

import com.shameel.bykeacasestudy.data.model.SearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WebService {

    @GET("search")
    suspend fun getSearchData(
        @Query("entity") entity: String,
        @Query("term") artistName: String,
        @Query("limit") searchResultLimit: String
    ): Response<SearchResponse>

}