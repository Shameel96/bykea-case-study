package com.shameel.bykeacasestudy.data

import com.shameel.bykeacasestudy.data.model.SearchResponse
import com.shameel.bykeacasestudy.data.remote.WebService
import com.shameel.bykeacasestudy.utils.Resource
import javax.inject.Inject

class DataRepository @Inject constructor(
    private val webService: WebService
) {
    suspend fun getSearchResults(artistName: String): Resource<SearchResponse> {
        return try {
            val response = webService.getSearchData(
                entity = "musicTrack",
                artistName = artistName,
                searchResultLimit = "25"
            )
            if (response.isSuccessful) {
                response.body()?.let {
                    return@let Resource.Success(it)
                } ?: Resource.Error("An unknown error occurred", null)
            } else {
                Resource.Error("An unknown error occurred", null)
            }
        } catch (e: Exception) {
            Resource.Error("Couldn't reach the server. check your internet connection", null)
        }
    }
}