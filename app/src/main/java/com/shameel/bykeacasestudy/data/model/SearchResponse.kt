package com.shameel.bykeacasestudy.data.model

data class SearchResponse(
    var resultCount: Int,
    var results: List<SearchResult>
)