package com.shameel.bykeacasestudy.callback

import com.shameel.bykeacasestudy.data.model.SearchResult

interface MusicItemClickCallback {
    fun onMusicItemClicked(item:SearchResult)
}