package com.shameel.bykeacasestudy.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BykeaCaseStudy : Application()