package com.shameel.bykeacasestudy.dependencyInjection

import com.shameel.bykeacasestudy.contants.Constants.baseUrl
import com.shameel.bykeacasestudy.data.DataRepository
import com.shameel.bykeacasestudy.data.remote.WebService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModules {

    @Singleton
    @Provides
    fun provideWebService(): WebService {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .build()
            .create(WebService::class.java)
    }

    @Singleton
    @Provides
    fun provideDataRepository(
        webService: WebService
    ) = DataRepository(webService)

}