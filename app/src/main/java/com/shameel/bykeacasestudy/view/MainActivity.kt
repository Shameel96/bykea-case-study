package com.shameel.bykeacasestudy.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.shameel.bykeacasestudy.R
import com.shameel.bykeacasestudy.base.BaseActivity
import com.shameel.bykeacasestudy.databinding.ActivityMainBinding

class MainActivity : BaseActivity() {

    private val binding : ActivityMainBinding by binding(R.layout.activity_main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this

        Looper.myLooper()?.let {
            Handler(it).postDelayed({
                startActivity(Intent(this, MusicActivity::class.java))
                finish()
            }, 2000)
        }
    }
}