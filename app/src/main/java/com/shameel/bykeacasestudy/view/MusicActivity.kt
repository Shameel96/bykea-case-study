package com.shameel.bykeacasestudy.view

import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.shameel.bykeacasestudy.R
import com.shameel.bykeacasestudy.adapter.MusicItemAdapter
import com.shameel.bykeacasestudy.base.BaseActivity
import com.shameel.bykeacasestudy.callback.MusicItemClickCallback
import com.shameel.bykeacasestudy.data.model.SearchResponse
import com.shameel.bykeacasestudy.data.model.SearchResult
import com.shameel.bykeacasestudy.databinding.ActivityMusicBinding
import com.shameel.bykeacasestudy.utils.*
import com.shameel.bykeacasestudy.viewmodel.DataViewModel


class MusicActivity : BaseActivity(), MusicItemClickCallback {

    private val binding: ActivityMusicBinding by binding(R.layout.activity_music)

    private val viewModel: DataViewModel by viewModels()
    private var mediaPlayer: MediaPlayer? = MediaPlayer()
    private val musicAdapter = MusicItemAdapter()
    private var isPlaying = false
    var isPaused = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this

        musicAdapter.setCallback(callback = this)

        binding.etSearch.afterTextChanged { it ->
            if (it.length > 3) {
                //delaying code to prevent unambiguous multiple hits
                Looper.myLooper()?.let { looper ->
                    Handler(looper).postDelayed({
                        viewModel.getMusicItems(it)
                    }, 300)
                }

            }
        }

        viewModel.searchResponse.observe(this, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    binding.pb.gone()
                    response.data?.let {
                        if (it.resultCount > 0) setRecyclerView(it) else setZeroState()
                    }
                }
                is Resource.Error -> {
                    binding.pb.gone()
                    response.message?.let {
                        toast(it)
                    }
                }
                is Resource.Loading -> binding.pb.visible()
            }
        })


        binding.buttonAudioControl.setOnClickListener {
            if (!isPaused) {
                it.setBackgroundResource(android.R.drawable.ic_media_play)
                isPaused = true
                mediaPlayer?.pause()
            } else {
                it.setBackgroundResource(android.R.drawable.ic_media_pause)
                isPaused = false
                mediaPlayer?.start()
            }
        }
    }

    private fun setZeroState() {
        musicAdapter.clearList()
        binding.layoutPlayer.gone()
        binding.tvNoSongs.visible()
    }

    private fun setRecyclerView(it: SearchResponse) {

        binding.tvNoSongs.gone()

        musicAdapter.setImagesList(imageList = it.results as ArrayList<SearchResult>)

        binding.rvSongs.apply {
            layoutManager = LinearLayoutManager(this@MusicActivity)
            adapter = musicAdapter
            addItemDecoration(
                DividerItemDecoration(
                    this@MusicActivity,
                    DividerItemDecoration.VERTICAL
                )
            )
        }

    }

    override fun onMusicItemClicked(item: SearchResult) {
        if (isPlaying) {
            isPlaying = false
            mediaPlayer?.release()
            mediaPlayer = null
        }

        mediaPlayer = MediaPlayer()
        mediaPlayer?.let {
            it.setDataSource(item.previewUrl)
            it.prepareAsync()
            it.setOnPreparedListener { mp ->
                mp.start()
                binding.layoutPlayer.visible()
                isPlaying = true
            }
        }
    }
}